from distutils.core import setup
import djangoviewutils

setup(name='django-viewutils',
      packages=['djangoviewutils'],
      description='Simple utils used in django views.',
      author='Franz Eichhorn',
      author_email='frairon@googlemail.com',
      url='https://bitbucket.org/eh14/django-viewutils',
      license='MIT',
      version=djangoviewutils.__version__,
      )
