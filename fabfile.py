from fabric.decorators import task
from fabric.tasks import execute
from fabric.operations import local

@task
def check():
    pass

@task
def localInstall():
    execute(check)
    local('sudo python setup.py install')